import { h, Component, ComponentProps } from 'preact';
import { Editor } from "../components/editor";

export interface FrontpageProps extends ComponentProps<Frontpage> {

}

export interface FrontpageState {}

export class Frontpage extends Component<FrontpageProps, FrontpageState> {


    render(props, state) {
        return <div>
            <section class="section">
                <div class="container">
                    <h1 class="title">Microbit Python editor {props.version === "beta" ? "Beta" : "v"+props.version}</h1>
                </div>
            </section>

            <section class="section">
                <div class="container">
                    <Editor version={props.version} />
                </div>
            </section>
        </div>
    }
}