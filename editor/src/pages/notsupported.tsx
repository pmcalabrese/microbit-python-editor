import { h, Component, ComponentProps } from 'preact';
import { Versions } from "../components/versions";

export interface NotSupportedProps extends ComponentProps<NotSupported> {}

export interface NotSupportedState {}

export class NotSupported extends Component<NotSupportedProps, NotSupportedState> {

    render() {
        return <div>
            <section class="section">
                <div class="container">
                    <h1 class="title">Auch. This version is not supported.</h1>
                    <h3>Try: <Versions /></h3>
                </div>
            </section>
        </div>
    }

}