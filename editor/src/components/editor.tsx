import { h, Component } from 'preact';
import autobind from 'autobind-decorator';
import { VERSIONS } from "../const";
import { HTMLInputEvent, FileReaderEvent } from "../interfaces";
import { Controls } from "../components/controls";
import { TextEditor } from "../components/text_editor";
import { Versions } from "../components/versions";

export interface AppState {
    version: string;
    content_of_the_file: string;
    version_of_the_file: string;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

export class Editor extends Component<any, AppState> {

    componentWillUpdate(props) {
        this.updateVersion(props);
    }

    componentDidMount() {
        this.updateVersion(this.props);
    }

    @autobind
    updateVersion(props) {
        if (VERSIONS.includes(props.version)) {
            this.setState({
                version: props.version
            })
        } else {
            if (isNumeric(props.version)) {
                // find the latest with that mayor version
                let minor;
                VERSIONS.forEach((version) => {
                    if (version.startsWith(props.version)) {
                        const _minor = version.split(".")[1];
                        if (!minor || _minor > minor) {
                            minor = _minor;
                        }
                    }
                });
                this.setState({
                    version: `${props.version}.${minor}`
                });
            } else {
                // redirect
            }
        }
    }

    @autobind
    loadFile(file) {
        let rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    let allText = rawFile.responseText;
                    alert(allText);
                }
            }
        }
        rawFile.send(null);
    }

    @autobind
    readFile(f, cb) {
        if (f) {
            var r = new FileReader();
            r.onload = function (e: FileReaderEvent) {
                var contents = e.target.result;
                cb(f, contents)
            }
            r.readAsText(f);
        } else {
            alert("Failed to load file");
        }
    }

    private getVersionFromContent(contents: string) {
        let version_of_the_file = contents.split("\n")[0].split(" v")[1];
        if (version_of_the_file.indexOf("|")) {
            const versions_of_the_file = version_of_the_file.split("|")
            version_of_the_file = versions_of_the_file[versions_of_the_file.length - 1]
        }
        return version_of_the_file;
    }

    @autobind
    fileChange(e: HTMLInputEvent) {
        const file = e.target.files[0];
        if (file.name.endsWith(".py")) {
            this.readFile(file, (f, contents) => {
                let version_of_the_file = this.getVersionFromContent(contents)
                this.setState({
                    version_of_the_file: version_of_the_file,
                    content_of_the_file: contents
                })
            });
        } else {
            alert("is not a Python file")
        }
    }

    @autobind
    save() {
        if (this.state.content_of_the_file) {
            const first_line = this.state.content_of_the_file.split("\n")[0];
            if (first_line.startsWith("#")) {
                if (this.state.version_of_the_file !== this.state.version) {
                    const new_first_line = first_line + "|" + this.state.version;
                    let content_in_lines = this.state.content_of_the_file.split("\n");
                    content_in_lines[0] = new_first_line;
                    const contents = content_in_lines.join("\n");
                    this.setState({
                        version_of_the_file: this.getVersionFromContent(contents),
                        content_of_the_file: contents
                    });
                    this.saveAs()
                }
            } else {
                // the first line is not a comment
            }
        } else {
            // the file is not loaded?
        }
    }

    @autobind
    saveAs() {
        var file_name = prompt("Please enter file name", "new_file.py");
        if (file_name && file_name.endsWith(".py")) {
            const blob = new Blob([this.state.content_of_the_file], { type: 'text/plain' })
            const anchor = document.createElement('a');
            anchor.download = file_name;
            anchor.href = (window.URL).createObjectURL(blob);
            anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
            anchor.click();
        }
        this.setState({

        })
    }

    render(props, state) {
        return <div>
            <span id="version">Versions: <Versions version={state.version} /></span>
            {state.version_of_the_file ? <span id="version">Version of the file v{state.version_of_the_file}</span> : null}
            <TextEditor content_of_the_file={state.content_of_the_file} />
            <Controls onChange={this.fileChange} onSave={this.save} content_of_the_file={state.content_of_the_file} />
        </div>
    }
}