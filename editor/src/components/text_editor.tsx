import { h } from 'preact';

export const TextEditor = ({ ...props }) => (
    <textarea name="textarea" id="textarea">{props.content_of_the_file}</textarea>
);