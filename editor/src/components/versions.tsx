import { h } from 'preact';
import { VERSIONS } from "../const";

export const Versions = ({ ...props }) => (
    <span>
        {VERSIONS.map((version) => {
            if (version === props.version) {
                return <b title="current version">v{props.version}</b>
            } else {
                return <a href={`/v/${version}`}><i>v{version}</i> </a>
            }
        })}
    </span>
);