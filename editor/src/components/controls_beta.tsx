import { h } from 'preact';

export const ControlsBeta = ({ ...props }) => (
    <div>
        <input type="file" name="one" value="" onChange={props.onChange} />
        <button disabled={!props.content_of_the_file} class="button" type="button" onClick={props.onSave}>Save Beta</button>
    </div>
);