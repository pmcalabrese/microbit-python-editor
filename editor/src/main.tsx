import { h, render, Component } from 'preact';
import { Router, route } from 'preact-router';
import { NotSupported } from "./pages/notsupported";
import { Frontpage } from "./pages/frontpage";

class Redirect extends Component<any, any> {
    componentWillMount() {
        route(this.props.to, true);
    }

    render() {
        return null;
    }
}

const Main = () => (
    <Router>
        <Frontpage path="v/:version" />
        <Redirect path="/" to="v/2.0" />
        <NotSupported default />
    </Router>
);

let app_id = 'app';
try {
    document.getElementById(app_id)
} catch (error) {
    throw Error(`Cannot find element with the id ${app_id}`);
}

render(<Main />, document.getElementById(app_id))